#ifndef TESTLIB_H
#define TESTLIB_H
#include<iostream>
#include<Matrix.h>
#include<OrbitData.h>
#include<string>
using namespace std;
class  TestLib
{

public:
    TestLib();
    int hello(int a=100);
    int matCalcTest();
    int filesIoTest(string fn="11");
    vector <double> ReadDataFile( string FileName );



};


#endif // TESTLIB_H
