#pragma once
#include <iostream>
#include <vector>
#include <cstring>
#include <math.h>
#include <stdio.h>
using namespace std;


class Matrix
{
public:
	double SquaredDistance();
	double Dot(Matrix const &other);

	Matrix();
	Matrix(int nRows, int nCols);
	Matrix(int nRows, int nCols, double value[]);
	Matrix(int nSize);
	Matrix(int nSize, double value[]);
	Matrix(const Matrix& other);
	~Matrix();


	bool printMat();

	bool Zero();
	bool Init(int nRows, int nCols);
	bool MakeUnitMatrix(int nSize);


	void SetData(double value[]);

	double GetElement(int nRow, int nCol) const;
	int	GetNumColumns() const;
	int	GetNumRows() const;
	double* GetData() const;
	vector <double> GetRowVector(int nRow) const;
	vector <double> GetColVector(int nCol) const;


	Matrix& operator=(const Matrix& other);
	bool operator==(const Matrix& other) const;
	bool operator!=(const Matrix& other) const;
	Matrix	operator+(const Matrix& other) const;
	Matrix	operator-(const Matrix& other) const;
	Matrix	operator*(double value) const;
	Matrix	operator/(double value) const;

	Matrix	operator*(const Matrix& other) const;


	bool CMul(const Matrix& AR, const Matrix& AI, const Matrix& BR, const Matrix& BI, Matrix& CR, Matrix& CI) const;
	Matrix Transpose() const;
	bool InvertGaussJordan();
	bool InvertGaussJordan(Matrix& mtxImag);
	bool InvertSsgj();
	bool InvertTrench();
	double DetGauss();
	int RankGauss();
	bool DetCholesky(double* dblDet);
	bool SplitLU(Matrix& mtxL, Matrix& mtxU);
	bool SplitQR(Matrix& mtxQ);
	bool SplitUV(Matrix& mtxU, Matrix& mtxV, double eps = 0.000001);
	void ppp(double a[], double e[], double s[], double v[], int m, int n);
	void sss(double fg[2], double cs[2]);
	bool GInvertUV(Matrix& mtxAP, Matrix& mtxU, Matrix& mtxV, double eps /*= 0.000001*/);
	bool MakeSymTri(Matrix& mtxQ, Matrix& mtxT, double dblB[], double dblC[]);
	bool SymTriEigenv(double dblB[], double dblC[], Matrix& mtxQ, int nMaxIt /*= 60*/, double eps /*= 0.000001*/);
	void MakeHberg();
	bool HBergEigenv(double dblU[], double dblV[], int nMaxIt /*= 60*/, double eps /*= 0.000001*/);
	bool JacobiEigenv(double dblEigenValue[], Matrix& mtxEigenVector, int nMaxIt = 60, double eps = 0.000001);
	bool JacobiEigenv2(double dblEigenValue[], Matrix& mtxEigenVector, double eps /*= 0.000001*/);

	bool SetElement(int nRow, int nCol, double value);
	bool SetElements(int nStartRow, int nStartCol, int nRow, int nCol, Matrix value);

	int	m_nNumColumns;
	int	m_nNumRows;
	double*	m_pData;
};

