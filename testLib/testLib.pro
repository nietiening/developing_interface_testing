#-------------------------------------------------
#
# Project created by QtCreator 2019-08-13T16:26:04
#
#-------------------------------------------------

QT       -= core gui

TARGET = testLib
TEMPLATE = lib

DEFINES += TESTLIB_LIBRARY

SOURCES += testlib.cpp \
    Matrix.cpp

HEADERS += testlib.h \
    Matrix.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
