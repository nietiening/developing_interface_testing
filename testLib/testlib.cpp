#include "testlib.h"

TestLib::TestLib()
{

}

int TestLib::hello(int a)
{
    cout<<"hi lib"<<endl;
    a++;
    cout<<"a = "<<a<<endl;
    cout<<"a*a = "<<a*a<<endl;
    return a*a;
}

int TestLib::matCalcTest()
//测试计算功能
{
    Matrix m(3,3);
    Matrix n (3,3);

    m.SetElement(0,0,1);  m.SetElement(0,1,0);  m.SetElement(0,2,0);
    m.SetElement(1,0,0);  m.SetElement(1,1,1);  m.SetElement(1,2,0);
    m.SetElement(2,0,0);  m.SetElement(2,1,0);  m.SetElement(2,2,1);
    cout<<"打印m矩阵 "<<endl;
    m.printMat();

    n.SetElement(0,0,3);  n.SetElement(0,1,0);  n.SetElement(0,2,0);
    n.SetElement(1,0,0);  n.SetElement(1,1,2);  n.SetElement(1,2,0);
    n.SetElement(2,0,0);  n.SetElement(2,1,0);  n.SetElement(2,2,1);
    cout<<"打印n矩阵 "<<endl;
    n.printMat();

    cout<<"打印m*n "<<endl;
    Matrix r = n*m;
    r.printMat();

    cout<<"打印m*n的逆矩阵 "<<endl;
    r.InvertGaussJordan();
    r.printMat();
    return 0;

}



vector<double> TestLib::ReadDataFile(string FileName)
{
    vector  <double> RtnValue;
    double buf=0;
    fstream DataFile;


    DataFile.open(FileName.c_str(), ios::in);
    if ( !DataFile.is_open() )
    {
        cout<<(("Error opening file!!!!")) ;
        exit (776);
    }
    while (!DataFile.eof() )
    {
        if (DataFile>>buf)
            RtnValue.push_back(buf);
    }
    DataFile.close();
    return  RtnValue;
}
