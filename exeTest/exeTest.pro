TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += ../testLib/
LIBS += -L ../build-out/ -l testLib
SOURCES += main.cpp
