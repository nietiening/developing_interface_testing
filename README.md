# 开发接口兼容性测试

#### 介绍
用于测试CentOS7系统动态链接库（*.so）兼容性。



#### 软件架构
1. build-out  编译输出的执行文件和动态链接库文件
2. data       测试用数据
3. exeTest    兼容性测试执行文件源码
4. testLib    兼容性测试动态链接库源码

#### 安装教程
1. 请提前在/etc/ld.so.conf中添加../build-out文件夹路径，以便系统可以找到库文件
2. 编译testLib动态链接库
3. 编译exeTest执行文件
4. exeTest/main.cpp中，调用testLib库进行了“hello world”、“数值计算测试”、“文件IO测试”。